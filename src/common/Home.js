import React, {Component} from 'react';
import './home.less'
import {NavLink, Route, Switch} from "react-router-dom";
import Header from "./Header";
import Notes from "../note/Notes";
import NoteDetail from "../note/NoteDetail";
import CreateNote from "../note/CreateNote";

class Home extends Component {

  render() {
    return (
      <div id={'homeBox'}>
        <Header/>
        <Switch>
          <Route exact path={'/'} component={Notes} />
          <Route exact path={'/notes/create'} component={CreateNote} />
          <Route exact path={'/notes/:id'} component={NoteDetail} />
        </Switch>
      </div>
    );
  }
}

export default Home;